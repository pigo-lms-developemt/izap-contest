<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<div class="alert alert-success alert-dismissible fade in" role="alert" style="margin-top: 8%; margin-bottom: 8%;">
                    
                <center>
                    <strong style="font-size: 22px;">
                        <?php echo elgg_echo('izap-contest:results:title')?>
                    </strong>
                </center>
</div>
<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                    <h2>
                        <?php echo elgg_echo('izap-contest:results:list')?>
                         
                    </h2>
                    
                    <div class="clearfix"></div>
                  </div>
<table id="datatable" class="table table-striped table-bordered" style="margin-top: 6%;">
    <thead>
      <tr>
            <th>
               #
            </th>

            <th>
                <?php echo elgg_echo("usersettings:statistics:label:name"); ?>
            </th>

            <th>
                <?php echo elgg_echo("usersettings:statistics:label:email"); ?>
            </th>

            <th>
                 <?php echo elgg_echo("izap-contest:results:correct-answers"); ?>
            </th>

            <th>
                <?php echo elgg_echo("izap-contest:results:grade"); ?>
            </th>

            <th>
               <?php echo elgg_echo("izap-contest:results:status"); ?>
           </th>

            <th>
              <?php echo elgg_echo("izap-contest:results:options"); ?>
           </th>

      </tr>
    </thead>
<tbody>