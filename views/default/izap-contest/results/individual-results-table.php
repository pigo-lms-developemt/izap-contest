<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$user_information = $vars['user_information'];
$result = $vars['result'];
?>
<div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                    <h2>
                        <?php echo elgg_echo('izap-contest:individual_results:title')?>
                        <small>  
                            <b>
                            <?php
                            echo $user_information[0]->name;
                            ?>
                                </>
                        </small>
                    </h2>
                    
                    <div class="clearfix"></div>
                  </div>
<table id="student-result" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th><?php echo elgg_echo('izap-contest:individual_results:name')?></th>
                          <th><?php echo elgg_echo('izap-contest:individual_results:email')?></th>
                          <th><?php echo elgg_echo('izap-contest:individual_results:username')?></th>
                          <th><?php echo elgg_echo('izap-contest:individual_results:score')?></th>
                           <th><?php echo elgg_echo('izap-contest:individual_results:answers')?></th>
                          <th><?php echo elgg_echo('izap-contest:individual_results:minimun')?></th>
                          <th><?php echo elgg_echo('izap-contest:individual_results:date')?></th>
                           
                        </tr>
                      </thead>


                      <tbody>
                        <tr>
                          <td>
                            <?php
                            echo $user_information[0]->name;
                            ?> 
                          </td>
                          
                          <td>
                            <?php
                            echo $user_information[0]->email;
                            ?> 
                          </td>
                          
                          <td>
                            <?php
                            echo $user_information[0]->username;
                            ?> 
                          </td>
                          
                          <td>
                              <center>
                             <span class="badge bg-green"style="font-size:20px;">
                                 <?php echo ($result->total_percentage*0.1) ?>
                             </span>
                            </center>
                          </td>
                          
                          <td>
                              <center>
                             <span class="label label-primary" style="font-size:18px;">
                                 <?php echo $result->total_score ?>
                             </span>
                            </center>
                          </td>
                          
                          <td> 
                              <center>
                             <span class="badge bg-default"style="font-size:20px;">
                             <?php 
                              echo ($result->required_percentage*0.1) ;
                              ?>
                             </span>
                            </center>
                              
                              
                          </td>
                          <td>
                              <button type="button" class="btn btn-info btn-xs"><?php 
                              echo date("d/m/Y",$result->time_created) ;
                              ?></button>
                              <button type="button" class="btn btn-dark btn-xs">
                                  <?php 
                              echo date("H:i:s",$result->time_created) ;
                              ?></button>
                              
                          </td>
                           
                        </tr>
                       
                      </tbody>
                    </table>


 </div></br></br></br>
     </div>