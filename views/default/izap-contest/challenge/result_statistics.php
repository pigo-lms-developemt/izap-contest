<?php
/* * *************************************************
 * PluginLotto.com                                 *
 * Copyrights (c) 2005-2011. iZAP                  *
 * All rights reserved                             *
 * **************************************************
 * @author iZAP Team "<support@izap.in>"
 * @link http://www.izap.in/
 * Under this agreement, No one has rights to sell this script further.
 * For more information. Contact "Tarun Jangra<tarun@izap.in>"
 * For discussion about corresponding plugins, visit http://www.pluginlotto.com/forum/
 * Follow us on http://facebook.com/PluginLotto and http://twitter.com/PluginLotto
 */

$array = $vars['array'];
?>
 

<div class="row">
              <div class="col-md-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>
                        <?php echo elgg_echo('izap-contest:statistics:title'); ?>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      
                       
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">

                    <p><?php echo elgg_echo('izap-contest:statistics:description'); ?></p>

                  
                    <table id="student-answers-view" class="table table-striped projects">
                      <thead>
                        <tr>
                          <th style="width: 1%">
                          #
                          </th>
                          
                          <th style="width: 34%">
                              <?php echo elgg_echo('izap-contest:statistics:question'); ?>
                          </th>
                          
                          <th>
                              <?php echo elgg_echo('izap-contest:statistics:status'); ?>
                          </th>
                          
                          <th>
                              <?php echo elgg_echo('izap-contest:statistics:selected_answer'); ?>
                          </th>
                          
                          <th>
                              <?php echo elgg_echo('izap-contest:statistics:correct_answer'); ?>
                          </th>
                           
                        </tr>
                      </thead>
                      <tbody>
                          
                <?php
                $i = 0;
                if (is_array($array) && sizeof($array)):
                  foreach ($array as $quiz_guid => $stats):
                    $class = ($stats['is_correct']) ? 'correct_answer' : 'wrong_answer';
                    $status = ($stats['is_correct']) ? 'correct.png' : 'wrong.png';
                    
                    $button_class = ($stats['is_correct']) ? 'btn-success' : 'btn-danger';
                    $i++;
                ?>
                        <tr>
                          <td><?php echo $i; ?></td>
                          <td>
                            <?php echo ($stats['question']); ?>
                             
                          </td>
                          
                          <td>
                            <img src="<?php echo $CONFIG->wwwroot . '/mod/' . GLOBAL_IZAP_CONTEST_PLUGIN . '/_graphics/' . $status ?>" />
                          </td>
                          <td class="project_progress">
                              <button type="button" class="btn <?php echo $button_class; ?> btn-xs" style="font-weight: bold;">
                             <?php echo $stats['answer']; ?>
                                  </button>
                          </td>
                          <td>
                            <button type="button" class="btn btn-success btn-xs">
                            <?php echo $stats['correct_answer'] ?>
                            </button>
                          </td>
                          
                        </tr>
                          <?php
            endforeach;
          endif;
          ?>
                      </tbody>
                    </table>
                    

                  </div>
                </div>
              </div>
            </div>