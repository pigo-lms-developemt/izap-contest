<?php
/* * *************************************************
 * PluginLotto.com                                 *
 * Copyrights (c) 2005-2011. iZAP                  *
 * All rights reserved                             *
 * **************************************************
 * @author iZAP Team "<support@izap.in>"
 * @link http://www.izap.in/
 * Under this agreement, No one has rights to sell this script further.
 * For more information. Contact "Tarun Jangra<tarun@izap.in>"
 * For discussion about corresponding plugins, visit http://www.pluginlotto.com/forum/
 * Follow us on http://facebook.com/PluginLotto and http://twitter.com/PluginLotto
 */

// this is the result page of the completed challenge
$result = $vars['result'];
$obtained_percentage = $result->total_percentage;
$width = $obtained_percentage;
//$entities = elgg_get_entities(array('type' => 'user'));
/*foreach ($entities as $entity)
{
    if($entity->guid == $result->owner_guid)
    {
        $ $entity->name;
    }
    
}*/

$evaluation_user_guid = $result->owner_guid;                
$options = array(
    'type' => 'user',
    //'subtype' => 'blog',
   // 'full_view' => false,
   // 'list_type_toggle' => false,
    'wheres' => array("e.guid = {$evaluation_user_guid}"),
   // 'limit' => 30,
   // 'pagination' => true
);
$user_information = elgg_get_entities($options);

//echo '******************';
//echo var_dump($test);
//echo $test[0]->name;
//echo '******************';
    
if ($obtained_percentage <= 0) {
  $width = 1;
}

echo elgg_view('izap-contest/results/individual-results-table', 
                  array(
                      'user_information' => $user_information, 
                      'result' => $result
                      ));
?>




 

<?php echo elgg_view('page/elements/title', array('title' => elgg_echo('izap-contest:challenge:result') . ': ' . $result->title)); ?>
<div class="contentWrapper">
  <?php
  echo elgg_view(GLOBAL_IZAP_CONTEST_PLUGIN . '/challenge/result_statistics', array('array' => unserialize($result->description)));
  ?>
</div>

<div class="contentWrapper">
  <div class="progress_bar_wrapper">
    <b><?php echo elgg_echo('izap-contest:challenge:passing_percentage'); ?>: <?php echo $result->required_percentage ?>%</b>
    <div style="background-color: white; width: 80%">
      <div class="progress_bar" style="background-color: #00FF00; width: <?php echo $result->required_percentage ?>%;"></div>
    </div>
  </div>
  <div class="progress_bar_wrapper">
    <b><?php echo elgg_echo('izap-contest:challenge:obtained_percentage'); ?>: <?php echo $obtained_percentage ?>%</b>
    <div style="background-color: white; width: 80%">
      <div class="progress_bar" style="background-color: <?php echo ($result->status == 'passed') ? '#00FF00' : '#FF0000' ?>;width: <?php echo $width ?>%;">    </div>
    </div>
  </div>
</div>

<div class="contentWrapper">
  <b><?php echo elgg_echo('izap-contest:result:total'); ?>: <?php echo $result->total_score; ?></b>
  <?php if ($result->is_completed == 'no') { ?>
    <b class="un_completed"><em><?php echo elgg_echo('izap-contest:challenge:not_completed'); ?></em></b>
  <?php } ?>

  <a href="<?php echo izapbase::setHref(array('context' => GLOBAL_IZAP_CONTEST_CHALLENGE_PAGEHANDLER, 'action' => 'result', 'page_owner' => FALSE, 'vars' => array($result->container_guid, elgg_get_friendly_title($result->title)))) ?>" class="cancel_button">
    <?php //echo elgg_echo('izap-contest:challenge:my_results') ?>
  </a>
</div>
<?php //echo $result->owner_guid;

//$db_prefix = elgg_get_config('dbprefix');

echo elgg_view('izap-contest/results/datatables');

?>

<script>
    //jQuery2("#datatable").DataTable();
    
    
     $(document).ready(function() {
        // Append a caption to the table before the DataTables initialisation
     
    jQuery2('#student-result').DataTable( {
        dom: 'Brtip',
        responsive: true,
       // "scrollX": true,
        buttons: [
           
            {
                extend: 'excel',
                className: "btn btn-sm btn-success",
                text: 'Guardar en Excel',
                
             
            },
           
            {
                extend: 'print',
                customize: function ( win ) {
                    $(win.document.body).find('h1').css('font-size', '12px');
                    $(win.document.body).find('h1').css('text-align', 'center');
                    $(win.document.body)    
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                        );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', '10pt' );
                },
                className: "btn btn-sm btn-warning",
                text: 'Imprimir',
                orientation: 'landscape',
                title: 'Resultado de: <?php echo $result->title;?> del estudiante: <?php echo $user_information[0]->name?>',
                messageBottom: null
                
            },
             
           
        ]
    } );
    
    jQuery2('#student-answers-view').DataTable( {
        dom: 'Bfrtip',
        responsive: true,
       // "scrollX": true,
        buttons: [
           
            {
                extend: 'excel',
                className: "btn btn-sm btn-success",
                text: 'Guardar en Excel',
                
             
            },
           
            {
                extend: 'print',
                
                className: "btn btn-sm btn-warning",
                text: 'Imprimir',
                customize: function ( win ) {
                    $(win.document.body).find('h1').css('font-size', '12px');
                    $(win.document.body).find('h1').css('text-align', 'center');
                    $(win.document.body)    
                        .css( 'font-size', '10pt' )
                        .prepend(
                            '<img src="http://datatables.net/media/images/logo-fade.png" style="position:absolute; top:0; left:0;" />'
                        );
 
                    $(win.document.body).find( 'table' )
                        .addClass( 'compact' )
                        .css( 'font-size', '10pt' );
                },
                title: 'Hoja de respuestas de: <?php echo $result->title;?> | Estudiante: <?php echo $user_information[0]->name?>',
                messageBottom: null
                
            },
             
           
        ]
    } );
    
    
} );
</script>
<?php
unset($_SESSION['challenge'][$vars['contest']]);
unset($_SESSION['proper_started'][$vars['contest']]);
?>

